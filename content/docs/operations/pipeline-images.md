---
title: Updating pipeline images
description: |
    How to update the container images used in the pipeline
---

The pipeline uses container images from the [container image repository] with
the `:production` tag.

For some images, this tag is created updated on the default branch.
This is the case for all images that have `AUTOMATIC_DEPLOYMENT` set to `true`
in [.gitlab-ci.yml].

For others, it is necessary to create the `:production` tags via the manual
deployment jobs as described below.

## Steps to update container images via MR

1. On the merge request in the [container image repository], check that the
   merge commit used for the latest pipeline includes all changes from the
   target branch as well.
1. Trigger the bot for the container images that should be updated via
   `@cki-ci-bot please test`.
1. Wait for the pipelines to finish and verify everything is correct.
1. Request a review of the merge request.
1. After approval, create the `:production` tags for the verified container
   images by clicking on the corresponding little arrows in the environment
   list.

## Steps to update container images without MR

1. Trigger a new pipeline via the [pipelines page] in the [container image
   repository]. After clicking on `Run pipeline`, optionally limit the pipeline
   to a certain container image by setting the `ONLY_JOB_NAME` variable to the
   name of the container image that should be updated, e.g. `builder-stream9`.
1. Wait for the parent and child pipelines to finish and verify everything is
   correct.
1. In a merge request in any [cki-project] project with the `cki-ci-bot`
   enabled, trigger the bot for the container images with the ID of the parent
   pipeline via `@cki-ci-bot please test [image_tag=g-123456789]`.
1. Wait for the pipelines to finish and verify everything is correct.
1. Create the `:production` tags for the verified container images by clicking
   on the corresponding manual `public-tag` and `internal-tag` jobs.

[container image repository]: https://gitlab.com/cki-project/containers
[.gitlab-ci.yml]: https://gitlab.com/cki-project/containers/-/blob/main/.gitlab-ci.yml
[pipelines page]: https://gitlab.com/cki-project/containers/-/pipelines
[cki-project]: https://gitlab.com/cki-project
