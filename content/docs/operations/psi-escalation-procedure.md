---
title: PSI escalation procedure
description: How to escalate PSI infrastructure problems
---

## Problem

When PSI infrastructure fails, problems should be escalated in a structured way.

## Steps

1. File a [SNOW ticket]:
   - Search for 'PnT report an issue'
   - Impact: 3 - Affects multiple teams
   - Urgency: 2 - No workaround; blocks business-critical processes
   - Application: DevOps - PSI-OCP (or correct application)
   - Support group: Openshift PNT (or correct group)

1. If after an hour, no response on SNOW ticket
   - Poke someone on the [exd-infra-escalation] Google Chat channel

This flow should only be used for real problems and not one-off failures.
A good time frame is to verify that a problem is occurring consistently for
~15mins (and verify it's really caused by PSI OpenShift) before submitting the
initial ticket/pinging people on the Google Chat channel.

Make sure to also add the outage to the [outage spreadsheet].

[SNOW ticket]: https://helpdesk.redhat.com
[exd-infra-escalation]: https://chat.google.com/room/AAAA6BChWkY
[outage spreadsheet]: https://docs.google.com/spreadsheets/d/1jeFI_AY56JkAQ6MRhGE0h5ckQU47ew4UJPQ76il3N-g/edit
