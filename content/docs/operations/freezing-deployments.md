---
title: "Freezing deployments"
description: >
  How to stop continuous deployment for a while
---

## Problem

You want to work on services or their deployments but do not want these changes
to be deployed, e.g. because of an outage.

## Steps

1. On [cee/deployment-all], go to the [CI/CD settings], and open the "Deploy
   freezes" section.

1. Add a deploy freeze with the following information:

   ```plain
   Freeze start: 0 0 * * *
   Freeze end: 59 23 * * *
   Time zone: [UTC 0] UTC
   ```

   From this moment on, no newly created pipelines will have any deployment
   jobs.

1. To reenable deployments, delete the deploy freeze and [run a complete
   deployment pipeline].

[cee/deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all
[CI/CD settings]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/settings/ci_cd
[run a complete deployment pipeline]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/pipelines/new
