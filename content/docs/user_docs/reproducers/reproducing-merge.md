---
title: Reproducing git checkout creation
linkTitle: git checkout
description: How to reproduce checkout creation done in the CKI pipeline
weight: 10
---

The reproducer documentation works with information provided in DataWarehouse
checkout views. If you got to the run from other places, you need to first
access this view:

- If you got a reporter email: A direct link to the checkout is provided
  towards the top of the email

![Reporter text example](known-issues-pass.png)

- If you got a GitLab CI pipeline: A direct link to the checkout is provided
  at the bottom of every pipeline job logs

![S3 artifacts and DataWarehouse links](s3-artifacts-links.png)

## Pipeline for a git branch

All information is available in the checkout view in DataWarehouse:

![Header of a checkout](dw-header-checkout.png)

```bash
git clone <repository>
git checkout <commit_hash>
```

## Merge request pipeline

If your local clone refers to the main repository differently than `origin`, do
substitute `origin` in the examples below for this reference.

### No changes since the pipeline execution

The pipeline grabs the reference of the MR merged into the target branch,
provided by GitLab. If neither the target branch nor the MR have changed since
the pipeline execution, the following code gives you the merged code:

```bash
git clone <repository>
git fetch origin merge-requests/<MR_ID>/merge
git checkout [-b <NEW_BRANCH_NAME>] FETCH_HEAD
```

### The MR or the target branch changed

If the code of either the target branch or the MR has been modified (e.g. new
commits have been merged into the target branch), the `/merge` reference points
to the updated version and not to the code that was used in the pipeline. To
get the correct references to merge locally, you have to look them up in the
pipeline.

> NOTE: The CKI team is planning to improve the experience of this step. This
> page will be updated once the required information are available in all
> places.

Get to the main pipeline view (link is provided from DataWarehouse, or you can
open it from the MR web UI) and open the `merge` stage job. Look for
`Validating merge commit` information:

![MR commit validation](mr-commit-validation.png)

```bash
git clone <repository>
git fetch origin <merge_commit_id>
git checkout FETCH_HEAD
```
